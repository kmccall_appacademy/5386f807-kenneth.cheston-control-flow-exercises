# EASY



# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str = str.chars.reject {|letter| letter == letter.downcase}
  str.join
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length.even?
      return str[(((str.length) / 2) - 1)..((str.length) / 2)]
  else return str[((str.length) / 2)]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.chars.count {|letter| VOWELS.include?(letter)}

end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  string = ""
  arr.each do |el|
    string << el + separator
  end
  return string[0...-1] unless string[-1] == arr[-1]
  return string
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
string = ""
  str.chars.each_with_index do |letter, idx|
    if (idx + 1).even?
      string << letter.upcase
    else string << letter.downcase
    end
  end
  string
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  result = str.split(' ').each {|word| word.reverse! if word.length >= 5}
  result.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)

  result = []
  (1..n).each_with_index do |num, idx|
    if num % 3 == 0 && num % 5 == 0
      result << "fizzbuzz"
    elsif num % 3 == 0
      result << "fizz"
    elsif num % 5 == 0
      result << "buzz"
    else result << num
    end
  end
  result
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  final_count = (1..num).count {|number| num % number == 0}
  final_count == 2
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  (1..num).select {|number| num % number == 0}
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  (1..num).select do |number|
    next if num % number != 0
    factor_count = (1..number).count {|number_2| number % number_2 == 0}
    factor_count == 2
  end
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  (1..num).count do |number|
    next if num % number != 0
    factor_count = (1..number).count {|number_2| number % number_2 == 0}
    factor_count == 2
  end
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  tally = arr.count {|num| num.even?}
  if tally == 1
    arr = arr.select {|num| num.even?}
    arr[0]
  else
    arr = arr.select {|num| num.odd?}
    arr[0]
  end
end
